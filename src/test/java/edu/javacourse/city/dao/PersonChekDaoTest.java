package edu.javacourse.city.dao;

import edu.javacourse.city.domain.PersonRequest;
import edu.javacourse.city.domain.PersonResponse;
import edu.javacourse.city.exception.PersonCheckException;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;


public class PersonChekDaoTest {

    @Test
    public void checkPerson() throws PersonCheckException {
        PersonRequest pr = new PersonRequest ();

        pr.setSurName ( "Васильев" );
        pr.setGivenName ( "Павел" );
        pr.setPatronymic ( "Николаевич" );
        pr.setDateOfBirth ( LocalDate.of ( 1995,3,18) );
        pr.setStreet_code ( 1 );
        pr.setBuilding ( "10" );
        pr.setExtension ( "2" );
        pr.setApartment ( "121" );

        PersonChekDao dao = new PersonChekDao ();
        PersonResponse ps = dao.checkPerson ( pr );
        Assert.assertTrue ( ps.isRegistered () );
        Assert.assertFalse ( ps.isTemporal () );
    }

    @Test
    public void checkPerson2() throws PersonCheckException {
        PersonRequest pr = new PersonRequest ();

        pr.setSurName ( "Васильева" );
        pr.setGivenName ( "Ирина" );
        pr.setPatronymic ( "Петровна" );
        pr.setDateOfBirth ( LocalDate.of ( 1997,8,21) );
        pr.setStreet_code ( 1 );
        pr.setBuilding ( "271" );
        pr.setApartment ( "4" );


        PersonChekDao dao = new PersonChekDao ();
        PersonResponse ps = dao.checkPerson ( pr );
        Assert.assertTrue ( ps.isRegistered () );
        Assert.assertFalse ( ps.isTemporal () );
    }
}